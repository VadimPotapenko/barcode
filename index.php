<?php
header("Content-type: image/png");
require __DIR__.'/barcode-master/barcode.php';

define ('HOST', 'https://smertmashina.bitrix24.ru/rest/1/dxfxa3jsbi8oxo6v/');
define ('FIELD', 'UF_CRM_1582181124');
$generator = new barcode_generator();

if (isset($_REQUEST['data']['FIELDS']['ID'])) {
	$format = 'png';
	$symbology = 'code-39';
	$data = '255991';
	$options = '';

	/* Create bitmap image. */
	$image = $generator->render_image($symbology, $data, $options);
	imagepng($image, 'barcode.png');
	imagedestroy($image);

	$base64 = base64_encode(file_get_contents('barcode.png'));
	$deal = call('crm.deal.update', array('id' => $_REQUEST['data']['FIELDS']['ID'], 'fields' => array(FIELD => array('fileData' => array('barcode.png', $base64)))));
}

function call ($method, $data) {
	$result = file_get_contents(HOST.$method.'?'.http_build_query($data));
	return json_decode($result, 1);
}
